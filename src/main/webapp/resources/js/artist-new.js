$("form").submit(function(){
	try{
		$(this).guardarArtista();
	} catch (exception) {
		console.log(exception);
	}
	return false;
});

$.fn.guardarArtista = function(){
	var data = {};
	var url = "/hellocasette/api/artists/new";
	
	if ($("[name=id]").val().length > 0){
		data.id = $("[name=id]").val();
		url = "/hellocasette/api/artists/save";
	}
	
	data.name = $("[name=name]").val();
	data.image = $("[name=image]").val();
	data.biography = $("[name=biography]").val();
	data.year = $("[name=year]").val();
	
	$.ajax({
		url: url,
		data: data,
		type: "POST",
		success: function(r){
			showAlert(r > 0);
			
			if ($("[name=id]").val().length == 0 && r > 0){
				$("[name=id]").val(r);
				history.pushState(null, null, "/hellocasette/artists/" + r);
				$("[data-type=title]").html("Edici&oacute;n de artista");
			}
		}
	});
}

function showAlert(result){
	if (result){
		$("[data-type=alert-area]")
		.append('<div class="alert alert-success" style="display:none">' +
			'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="fa fa-check"></i>&nbsp;Los datos se han guardado correctamente.' +
			'</div>');
	} else {
		$("[data-type=alert-area]")
		.append('<div class="alert alert-danger" style="display:none">' +
			'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><i class="fa fa-times"></i>&nbsp;Los datos no se han guardado correctamente.' +
			'</div>');
	}
	
	$(".alert:last-child").slideDown('fast');
}

$(document).on("click", ".alert .close", function(){
	$(this).parents(".alert").slideUp('fast', function(){
		$(this).remove();
	});
});