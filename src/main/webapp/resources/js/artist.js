$("form").submit(function(){
	$(this).guardarArtista();
	
	return false;
});

$.fn.guardarArtista = function(){
	var data = {};
	
	data.id = $("[name=id]").val();
	data.name = $("[name=name]").val();
	data.image = $("[name=image]").val();
	data.biography = $("[name=biography]").val();
	data.year = $("[name=year]").val();
	
	$.ajax({
		url: "/hellocasette/api/artists/save",
		data: data,
		type: "POST",
		success: function(r){
			if (r){
				alert("Se ha guardado correctamente");
			}
		}
	});
}