var __order_field = "id";
var __order_orientation = 1;
function cargarArtistas(){
	var data = {};
	
	if ($("[name=filter]").val().length > 0)
		data.filter = $("[name=filter]").val();
	
	data.order = __order_field;
	data.orientation = __order_orientation;
	
	$.ajax({
		url: "/hellocasette/api/artists",
		type: "GET",
		dataType: "json",
		data: data,
		beforeSend: function(){
			$("table tbody").empty();	
		},
		success: function(r){
			var i = 0;
			if (r.length != null){
				while (i < r.length){
					var objectToAppend = 
						'<tr><td>' + r[i].id + '</td><td>' + 
						r[i].name + '</td><td><a href="/hellocasette/artists/' +
						r[i].id + '" class="btn btn-sm btn-default">' +
						'<i class="fa fa-pencil"></i>&nbsp;&nbsp;Editar</a></td></tr>';
					$("table tbody").append(objectToAppend);
					
					i++;
				}
			}
		}
	});
	
}

$("form").submit(function(){
	cargarArtistas();
	
	return false;
});

$("[data-type=btn-clean-filter]").click(function(event){
	$("[name=filter]").val("");
	cargarArtistas();
	
	event.preventDefault();
});

$("[data-type=filter]").click(function(event){
	var orderField = $(this).data("filter");
	if (orderField == __order_field && __order_orientation == 1)
		__order_orientation = 0;
	else
		__order_orientation = 1;
	
	__order_field = orderField;
	cargarArtistas();
	
	event.preventDefault();
});

cargarArtistas();