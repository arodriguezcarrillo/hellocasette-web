<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="includes/header.jsp" %>


<div class="row-fluid container" style="margin-top: 30px">
	<ol class="breadcrumb">
	  <li><a href="/hellocasette">Inicio</a></li>
	  <li><a href="/hellocasette/artists">Artistas</a></li>
	  <li class="active" data-type="title">Nuevo artista</li>
	</ol>
	<h1 data-type="title">Introducci�n de artista</h1>
	<div data-type="alert-area">
	</div>
	<form class="form-horizontal">
		<input type="hidden" name="id" />
		<div class="form-group">
			<label class="col-sm-2 control-label" for="name">Nombre:</label>
			<div class="col-sm-10">
				<input class="form-control" type="text" value="" name="name" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="name">Biograf�a:</label>
			<div class="col-sm-10">
				<textarea class="form-control" name="biography"></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="name">Imagen:</label>
			<div class="col-sm-10">
				<input class="form-control" type="text" value="" name="image" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="name">A�o de formaci�n:</label>
			<div class="col-sm-10">
				<input class="form-control" type="text" value="" name="year" />
			</div>
		</div>
		<div class="text-right">
			<a href="/hellocasette/artists" class="btn btn-danger"><i class="fa fa-angle-left"></i>&nbsp;&nbsp;Atr�s</a>
			<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Guardar</button>
		</div>
	</form>
</div>

<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="/hellocasette/resources/js/artist-new.js"></script>
