<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include file="includes/header.jsp" %>

<div class="row-fluid container" style="margin-top: 30px">
	<ol class="breadcrumb">
	  <li><a href="/hellocasette">Inicio</a></li>
	  <li class="active">Artistas</li>
	</ol>
	<h1>Listado de artistas<a href="/hellocasette/artists/new" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>&nbsp;Nuevo artista</a></h1>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4>&nbsp;&nbsp;Filtrado de artistas</h4>
		</div>
		<div class="panel-body">
			<form class="form-horizontal col-md-12">
				<div class="form-group">
					<input class="form-control" type="text" name="filter" placeholder="Filtrado" />
				</div>
				<div class="text-right">
					<a href="#" data-type="btn-clean-filter" class="btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp;&nbsp;Limpiar filtro</a>
					<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i>&nbsp;&nbsp;Aplicar filtro</button>
				</div>
			</form>
		</div>
	</div>
	<table class="table">
		<thead>
			<tr>
				<th><a href="#" data-filter="id" data-type="filter">Id</a></th>
				<th><a href="#" data-filter="name" data-type="filter">Nombre</a></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="/hellocasette/resources/js/artists.js"></script>
<%@include file="includes/footer.jsp" %>