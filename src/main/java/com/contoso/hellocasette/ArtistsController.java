package com.contoso.hellocasette;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.contoso.hellocasette.dao.ArtistDAO;
import com.contoso.hellocasette.entities.Artist;

@Controller
@RequestMapping(value="/artists")
public class ArtistsController {

	private ArtistDAO artistDAO;
	
	@Autowired
	@Qualifier(value="artistDAO")
	public void setArtistDAO(ArtistDAO artistDAO){
		this.artistDAO = artistDAO;
	}
	
	@RequestMapping
	public String AllArtists(Model model){
		return "artists";
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.GET)
	public String Artist(Model model, 
			@PathVariable("id") Integer id){
		
		model.addAttribute(
				"artist", this.artistDAO.getArtist(id));
		
		return "artist";
	}
	
	@RequestMapping(value="/new", method=RequestMethod.GET)
	public String NewArtist(){
		return "newartist";
	}
	
	
}
