package com.contoso.hellocasette.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.contoso.hellocasette.dao.ArtistDAO;
import com.contoso.hellocasette.entities.Artist;

@RestController
@RequestMapping(value="/api/artists")
public class ServiceArtistsController {

	ArtistDAO artistDAO;
	
	@Autowired
	@Qualifier(value="artistDAO")
	public void setArtistDAO(ArtistDAO artistDAO){
		this.artistDAO = artistDAO;
	}
	
	@RequestMapping
	public List<Artist> Artists(
		@RequestParam(value="filter",required=false) String filter,
		@RequestParam(value="order",required=false) String order,
		@RequestParam(value="orientation",required=false) Integer orientation){
		
		return this.artistDAO.getArtists(filter, order, orientation);
	}
	
	@RequestMapping(value = "{id}")
	public Artist Artist(@PathVariable("id") Integer id){
		return this.artistDAO.getArtist(id);
	}
	
	@RequestMapping(value="save", method=RequestMethod.POST)
	public Boolean SaveArtist( 
			@ModelAttribute Artist item
			){
		Boolean result = false;
		
		try{
			this.artistDAO.UpdateArtist(item);
			result = true;
		} catch (Exception excep) {
			
			result = false;
		}
		
		return result;
	}
	
	@RequestMapping(value="new", method=RequestMethod.POST)
	public Integer SaveNewArtist( 
			@ModelAttribute Artist item
			){
		Integer result = -1;
		
		try{
			this.artistDAO.NewArtist(item);
			result = item.getId();
		} catch (Exception excep) {
			
			result = -1;
		}
		
		return result;
	}
}
