package com.contoso.hellocasette.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.contoso.hellocasette.entities.Artist;

@Repository
public class ArtistDAO {

	private static final Logger logger = LoggerFactory.getLogger(ArtistDAO.class);
	
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	

	@Transactional
    public List<Artist> getArtists(String filter, String order, Integer orientation) {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = null;
        String queryString = "from Artist";
        if (filter != null && filter.length() > 0){
        		queryString = "from Artist where name like :name";
        		query.setParameter("name", "%" + filter + "%");
        } else
        		queryString = "from Artist";
        
        if (order != null && order.length() > 0){
        		queryString += " order by " + order;
        		
        		if (orientation == 0)
        			queryString += " desc ";
        }
        
        query = session.createQuery(queryString);
        List<Artist> artists = query.list();

        return artists;
    }
	
	@Transactional
	public Artist getArtist(Integer id){
		Session session = this.sessionFactory.getCurrentSession();
		List<Artist> result = session.createQuery("from Artist where id = :id")
			.setInteger("id", id).list();
		
		if (result.size() > 0)
			return result.get(0);
		else
			return null;
	}
	
	@Transactional
	public Boolean UpdateArtist(Artist item){
		Boolean result = false;
		
		try{
			Session session = this.sessionFactory.getCurrentSession();
			session.update(item);
			result = true;
		} catch (Exception excep){
			result = false;
		}
		
		return result;
	}
	
	@Transactional
	public Boolean NewArtist(Artist item){
		Boolean result = false;
		
		try{
			Session session = this.sessionFactory.getCurrentSession();
			session.save(item);
			result = true;
		} catch (Exception excep){
			result = false;
		}
		
		return result;
	}
}
